angular.module('flapperNews', ['ui.router'])
.config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider){
  $stateProvider
    .state('home', {
      url: '/home',
      templateUrl: 'home.html',
      controller: 'MainCtrl as main'
    })
    .state('posts', {
      url: '/posts/{id}',
      templateUrl: 'posts.html',
      controller: 'PostsCtrl as posts'
    })
  $urlRouterProvider.otherwise('home');
}])

.factory('posts', [function(){
  var o = {
    posts: [{title: "post 1", upvotes: 0}]
  };
  return o;
}])

.controller('MainCtrl', ['posts', function(posts){
  this.posts = posts.posts;
  this.addPost = function(){
    if(!this.title || this.title === '') { return; }
    this.posts.push({
      title: this.title,
      link: this.link,
      upvotes: 0,
      comments: [
        {author: 'Joe', body: 'Cool post!', upvotes: 0},
        {author: 'Bob', body: 'Great idea but everything is wrong!', upvotes: 0}
      ]
    });
    this.title = '';
    this.link = '';
  }
  this.incrementUpvotes = function(post) {
    post.upvotes += 1;
  };
}]);
.controller('PostsCtrl', ['$stateParams', 'posts', function($stateParams, posts){
  this.post = posts.posts[$stateParams.id];
  this.addComment = function(){
    if(this.body === '') { return; }
    this.post.comments.push({body: this.body, author: 'user', upvotes: 0});
    this.body = '';
  };
}]);
